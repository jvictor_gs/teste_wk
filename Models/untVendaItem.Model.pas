unit untVendaItem.Model;

interface

uses
  untPadrao.Model;

  type TVendaItemModel = class(TModel)
    private

    public
      constructor Create;
      destructor Destroy; override;

      procedure SetTableAutoIncrement; override;
  end;

implementation

{ TVendaItemModel }

constructor TVendaItemModel.Create;
begin
  inherited;

end;

destructor TVendaItemModel.Destroy;
begin

  inherited;
end;


procedure TVendaItemModel.SetTableAutoIncrement;
begin
  inherited;
  FTabelaAutoIncremento := True;
end;

end.
