unit untVenda.Model;

interface

uses
  untPadrao.Model;

  type TVendaModel = class(TModel)
    private

    public
      constructor Create;
      destructor Destroy; override;

      procedure SetTableAutoIncrement; override;
  end;


implementation

{ TVendaModel }

constructor TVendaModel.Create;
begin
  inherited;
end;

destructor TVendaModel.Destroy;
begin

  inherited;
end;

procedure TVendaModel.SetTableAutoIncrement;
begin
  inherited;
  Self.FTabelaAutoIncremento := False;
end;

end.
