unit untPadrao.Model;

interface

uses FireDAC.Comp.Client, System.Classes, System.SysUtils,
  Data.DB, untModel.Interfaces, untConnection, untDataModule.Firedac;

  type TModel = class(TInterfacedObject, IModel)
    protected
      type TScriptInsert = record
        Fields: string;
        Values: string;
      end;

      type TScriptUpdate = record
        Expressao: string;
        PrimaryKey: string;
        ValuePK: string;
      end;

      type TScriptDelete = record
        PrimaryKey: string;
        ValuePK: string;
      end;

    private
      FDataAccessObject: IConexao;
      FDataModule: TDMFiredac;
      function FieldExists(Tabela: string; FieldName: string): Boolean;
      function GetColumnsTable(Tabela: string): TStringList;
      function AssembleScriptInsert(Sender: TDataSet; Table: string): TScriptInsert;
      function AssembleScriptUpdate(Sender: TDataSet; Table: string): TScriptUpdate;
      function AssembleScriptDelete(Sender: TDataSet; Table: string): TScriptDelete;
    public
      FTabelaAutoIncremento: Boolean;
      constructor Create;
      destructor Destroy; override;

      function GetSeq(Table: string): Integer;
      function GetPrimaryKey(Table: string): string;
      function CreateConnection: TFDConnection;
      function CreateDataSet(Sql: string): TFDQuery;
      function GetScriptInsert(Sender: TDataSet; Table: string): string;
      function GetScriptUpdate(Sender: TDataSet; Table: string): string;
      function GetScriptDelete(Sender: TDataSet; Table: string): string;

      procedure Execute(Sql: string);
      procedure SetTableAutoIncrement; virtual;
  end;

implementation

uses WK.Lib.Strings;

{ TModel }

constructor TModel.Create;
begin
  FDataModule := TDMFiredac.Create(nil);
  FDataAccessObject := TConexaoMySQL.Create;
  Self.SetTableAutoIncrement;
end;

destructor TModel.Destroy;
begin
  FreeAndNil(FDataModule);
  inherited;
end;

function TModel.CreateConnection: TFDConnection;
begin
  Result := FDataAccessObject.GetConnection;
end;

function TModel.CreateDataSet(Sql: string): TFDQuery;
begin
  Result := TFDQuery.Create(nil);
  Result.Connection := FDataAccessObject.GetConnection;
  Result.CachedUpdates := True;
  Result.UpdateOptions.UpdateNonBaseFields := True;
  Result.SQL.Clear;
  Result.SQL.Add(Sql);
  Result.Open;
end;

function TModel.GetColumnsTable(Tabela: string): TStringList;
var
  vInstruction: string;
  oQuery: TFDQuery;
begin
  Result := TStringList.Create;
  vInstruction := FDataAccessObject.GetSelectSchemaCollumns(Tabela);
  oQuery := CreateDataSet(vInstruction);
  try
    oQuery.Open;
    oQuery.First;
    while not oQuery.Eof do
    begin
      Result.Add(oQuery.FieldByName('COLUMN_NAME').AsString);
      oQuery.Next;
    end;
  finally
    FreeAndNil(oQuery);
  end;
end;

function TModel.GetPrimaryKey(Table: string): string;
begin
  Result := FDataAccessObject.GetPrimaryKey(Table);
end;

function TModel.GetSeq(Table: string): Integer;
const
  cInstruction = 'SELECT MAX(%s) AS PRIMARYKEY FROM %s;';
var
  oQuery: TFDQuery;
  vPrimaryKey: string;
begin
  vPrimaryKey := GetPrimaryKey(Table);
  oQuery := CreateDataSet(Format(cInstruction, [vPrimaryKey, Table]));
  try
    oQuery.Open;
    if oQuery.IsEmpty then
      Result := 1
    else
      Result := StrToIntDef(oQuery.FieldByName('PRIMARYKEY').AsString, 0) + 1;
  finally
    FreeAndNil(oQuery);
  end;
end;

function TModel.FieldExists(Tabela: string; FieldName: string): Boolean;
begin
  Result := FDataAccessObject.FieldExists(Tabela, FieldName);
end;

function TModel.AssembleScriptInsert(Sender: TDataSet; Table: string): TScriptInsert;
var
  vField: TField;
  vColumns, vValues: string;
  vListColumnsTable: TStringList;
begin
  vColumns := EmptyStr;
  vValues := EmptyStr;
  vListColumnsTable := GetColumnsTable(Table);

  for vField in Sender.Fields do
  begin
    if vListColumnsTable.IndexOf(Sender.Fields[vField.Index].FieldName) <> -1 then
    begin
      if (FTabelaAutoIncremento) and (Sender.Fields[vField.Index].FieldName = Self.GetPrimaryKey(Table)) then
        Continue;
      vColumns := vColumns + Sender.Fields[vField.Index].FieldName + ', ';
      vValues := vValues + FormatStringToDataBase(
                             Sender.Fields[vField.Index].AsString,
                             Sender.FIelds[vField.Index].DataType) + ', ';
    end;
  end;

  Result.Fields := Copy(vColumns, 0, vColumns.Length-2);
  Result.Values := Copy(vValues, 0, vValues.Length-2);
end;

function TModel.AssembleScriptUpdate(Sender: TDataSet; Table: string): TScriptUpdate;
var
  vField: TField;
  vExpressao: string;
begin
  vExpressao := EmptyStr;
  for vField in Sender.Fields do
  begin
    if FieldExists(Table, Sender.Fields[vField.Index].FieldName) then
    begin
      vExpressao := vExpressao + Sender.Fields[vField.Index].FieldName + ' = ' +
                               FormatStringToDataBase(
                                 Sender.Fields[vField.Index].AsString,
                                 Sender.FIelds[vField.Index].DataType) + ', ';
    end;
  end;
  Result.PrimaryKey := GetPrimaryKey(Table);
  Result.ValuePK := Sender.FieldByName(Result.PrimaryKey).AsString;
  Result.Expressao := Copy(vExpressao, 0, vExpressao.Length-2);
end;

procedure TModel.SetTableAutoIncrement;
begin
  FTabelaAutoIncremento := False;
end;

function TModel.AssembleScriptDelete(Sender: TDataSet; Table: string): TScriptDelete;
begin
  Result.PrimaryKey := GetPrimaryKey(Table);
  Result.ValuePK := Sender.FieldByName(Result.PrimaryKey).AsString;
end;

function TModel.GetScriptInsert(Sender: TDataSet; Table: string): string;
const
  cInstruction = 'INSERT INTO %s (%s) VALUES (%s);';
var
  vScript: TScriptInsert;
begin
  vScript := AssembleScriptInsert(Sender, Table);
  Result := Format(cInstruction, [Table, vScript.Fields, vScript.Values]);
end;

function TModel.GetScriptUpdate(Sender: TDataSet; Table: string): string;
const
  cInstruction = 'UPDATE %s SET %s WHERE %s = %s;';
var
  vScript: TScriptUpdate;
begin
  vScript := AssembleScriptUpdate(Sender, Table);
  Result := Format(cInstruction, [Table, vScript.Expressao, vScript.PrimaryKey, vScript.ValuePK]);
end;

function TModel.GetScriptDelete(Sender: TDataSet; Table: string): string;
const
  cInstruction = 'DELETE FROM %s WHERE %s = %s;';
var
  vScript: TScriptDelete;
begin
  vScript := AssembleScriptDelete(Sender, Table);
  Result := Format(cInstruction, [Table, vScript.PrimaryKey, vScript.ValuePK]);
end;

procedure TModel.Execute(Sql: string);
begin
  FDataAccessObject.Execute(Sql);
end;

end.
