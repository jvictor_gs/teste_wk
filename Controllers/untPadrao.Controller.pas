unit untPadrao.Controller;

interface

uses
  untPadrao.Model, FireDAC.Comp.Client, System.Classes, System.SysUtils,
  Data.DB, untController.Interfaces, untModel.Interfaces;

  type TController = class(TInterfacedObject, IController)
    public
      FModel: IModel;
      FAutoCommit: Boolean;
      FScript: TStringList;

      constructor Create;
      destructor Destroy; override;

      function CreateDataSet(Sql: string): TDataSet;
      function GetSeq(Table: string): Integer;
      function GetPrimaryKey(Table: string): string;
      function AllowInsert(Sender: TDataSet): Boolean; virtual;
      function AllowDelete(Id: string): Boolean; virtual;
      function GetRequiredFields: TStringList; virtual; abstract;
      function GetSelectInstruction(Where: string): string; virtual; abstract;

      procedure Insert(Sender: TDataSet; Table: string);
      procedure Update(Sender: TDataSet; Table: string);
      procedure Delete(Sender: TDataSet; Table: string);
      procedure ApplyUpdates;
      procedure SetModel; virtual; abstract;
      procedure SetAutoCommit; virtual; abstract;

  end;

implementation

uses WK.Lib.Strings;

{ TController }

constructor TController.Create;
begin
  FScript := TStringList.Create;
  Self.SetModel;
  Self.SetAutoCommit;
end;

destructor TController.Destroy;
begin
  FreeAndNil(FScript);
  inherited;
end;

function TController.CreateDataSet(Sql: string): TDataSet;
begin
  Result := FModel.CreateDataSet(Sql);
end;

function TController.AllowInsert(Sender: TDataSet): Boolean;
var
  vRequiredFieldsList: TStringList;
  vIsValidated: Boolean;
  vField: TField;
begin
  vRequiredFieldsList := GetRequiredFields;

  Result := True;
  for vField in Sender.Fields do
  begin
    if vRequiredFieldsList.IndexOf(Sender.Fields[vField.Index].FieldName) <> -1 then
    begin
      vIsValidated := IsEmptyStr(Sender.Fields[vField.Index].AsString);
      if vIsValidated then
        Exit(False);
    end;
  end;
end;

function TController.GetPrimaryKey(Table: string): string;
begin
  Result := FModel.GetPrimaryKey(Table);
end;

function TController.GetSeq(Table: string): Integer;
begin
  Result := FModel.GetSeq(Table);
end;

function TController.AllowDelete(Id: string): Boolean;
begin
  Result := True;
end;

procedure TController.Insert(Sender: TDataSet; Table: string);
const
  cMessage = 'Existem campos obrigat�rios que n�o foram informados.';
var
  vScript: string;
begin
  if Self.AllowInsert(Sender) then
  begin
    if FAutoCommit then
    begin
      vScript := FModel.GetScriptInsert(Sender, Table);
      FModel.Execute(vScript);
    end
    else
      FScript.Add(FModel.GetScriptInsert(Sender, Table));
  end
  else
    raise Exception.Create(cMessage);
end;

procedure TController.ApplyUpdates;
var
  I:  Integer;
begin
  for I := 0 to pred(FScript.Count) do
    FModel.Execute(FScript.Strings[I]);
end;

procedure TController.Update(Sender: TDataSet; Table: string);
const
  cMessage = 'Existem campos obrigat�rios que n�o foram informados.';
var
  vScript: string;
begin
  if Self.AllowInsert(Sender) then
  begin
    if FAutoCommit then
    begin
      vScript := FModel.GetScriptUpdate(Sender, Table);
      FModel.Execute(vScript);
    end
    else
      FScript.Add(FModel.GetScriptUpdate(Sender, Table));
  end
  else
    raise Exception.Create(cMessage);
end;

procedure TController.Delete(Sender: TDataSet; Table: string);
const
  cMessage = 'N�o � poss�vel excluir! ' +
             'O registro possui depend�ncias com outras tabelas.';
var
  vScript, vId: string;
begin
  vId := Sender.FieldByName(FModel.GetPrimaryKey(Table)).AsString;
  if Self.AllowDelete(vId) then
  begin
    if FAutoCommit then
    begin
      vScript := FModel.GetScriptDelete(Sender, Table);
      FModel.Execute(vScript);
    end
    else
      FScript.Add(FModel.GetScriptDelete(Sender, Table));
  end
  else
    raise Exception.Create(cMessage);
end;

end.
