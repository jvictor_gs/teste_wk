unit untVenda.Controller;

interface

uses
  untPadrao.Controller, System.Classes, System.SysUtils, untVenda.Model;

  type TVendaController = class(TController)
    private

    public
      constructor Create;
      destructor Destroy; override;

      function GetRequiredFields: TStringList; override;
      function GetSelectInstruction(cCondition: string = ''): string; override;
      procedure SetModel; override;
      procedure SetAutoCommit; override;
  end;

implementation

{ TVendaController }

constructor TVendaController.Create;
begin
  inherited;

end;

destructor TVendaController.Destroy;
begin

  inherited;
end;

function TVendaController.GetRequiredFields: TStringList;
begin
  Result := TStringList.Create;
  Result.Add('IDVENDA');
  Result.Add('DESCRICAO');
  Result.Add('TOTALVENDA');
  Result.Add('DATAVENDA');
  Result.Add('IDCLIENTE');
end;

function TVendaController.GetSelectInstruction(cCondition: string): string;
const
  cSQL = 'SELECT V.IDVENDA, V.DESCRICAO, V.TOTALVENDA, V.DATAVENDA, '
        + 'V.NUMEROPEDIDO, C.IDCLIENTE, C.NOME '
        + 'FROM VENDA AS V '
        + 'LEFT JOIN CLIENTE AS C ON (C.IDCLIENTE = V.IDCLIENTE) '
        + '%s ORDER BY V.IDVENDA;';
var
  vWhere: string;
begin
  if cCondition = EmptyStr then
    vWhere := EmptyStr
  else
    vWhere := 'WHERE V.' + cCondition;

  Result := Format(cSQL, [vWhere]);
end;

procedure TVendaController.SetAutoCommit;
begin
  inherited;
  FAutoCommit := True;
end;

procedure TVendaController.SetModel;
begin
  inherited;
  FModel := TVendaModel.Create;
end;

end.
