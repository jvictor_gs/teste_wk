unit untVendaItem.Controller;

interface

uses
  untPadrao.Controller, System.Classes, untVendaItem.Model, System.SysUtils,
  Data.DB;

  type TVendaItemController = class(TController)
    private

    public
      constructor Create;
      destructor Destroy; override;

      function GetRequiredFields: TStringList; override;
      function GetSelectInstruction(Condicao: string = ''): string; override;
      procedure SetModel; override;
      procedure SetAutoCommit; override;

      function CalcularTotalItem(Sender: TDataSet): Currency; overload;
      function CalcularTotalItem(pQuantidade: Integer;
        pValorUnitario: Currency): Currency; overload;
      function CalcularTotalVenda(Sender: TDataSet): Currency;
  end;

implementation

{ TVendaItemController }

function TVendaItemController.CalcularTotalVenda(Sender: TDataSet): Currency;
begin
  Result := 0;
  Sender.First;
  while not Sender.Eof do
  begin
    Result := Result + Sender.FieldByName('VALORTOTAL').AsCurrency;
    Sender.Next;
  end;
end;

function TVendaItemController.CalcularTotalItem(pQuantidade: Integer;
  pValorUnitario: Currency): Currency;
begin
  Result := pQuantidade * pValorUnitario;
end;

function TVendaItemController.CalcularTotalItem(Sender: TDataSet): Currency;
begin
  with Sender do
  begin
    Result := CalcularTotalItem(
                StrToIntDef(FieldByName('QUANTIDADE').AsString, 0),
                StrToCurrDef(FieldByName('VALORUNITARIO').AsString, 0));
  end;
end;

constructor TVendaItemController.Create;
begin
  inherited;

end;

destructor TVendaItemController.Destroy;
begin

  inherited;
end;

function TVendaItemController.GetRequiredFields: TStringList;
begin
  Result := TStringList.Create;
  Result.Add('IDVENDA');
  Result.Add('IDPRODUTO');
  Result.Add('QUANTIDADE');
  Result.Add('VALORUNITARIO');
  Result.Add('VALORTOTAL');
  Result.Add('IDEMPRESA');
  Result.Add('IDUSUARIO');
  Result.Add('DATAREGISTRO');
end;

function TVendaItemController.GetSelectInstruction(Condicao: string): string;
const
  SQL = 'SELECT VI.IDVENDAITEM, VI.IDVENDA, VI.IDPRODUTO, P.DESCRICAO AS PRODUTO, '
        + 'VI.QUANTIDADE, VI.VALORUNITARIO, VI.VALORTOTAL '
        + 'FROM VENDAITEM AS VI '
        + 'LEFT JOIN PRODUTO AS P ON (P.IDPRODUTO = VI.IDPRODUTO) '
        + '%s ORDER BY VI.IDVENDAITEM;';
var
  Where: string;
begin
  if Condicao = EmptyStr then
    Where := EmptyStr
  else
    Where := 'WHERE VI.' + Condicao;

  Result := Format(SQL, [Where]);
end;

procedure TVendaItemController.SetAutoCommit;
begin
  inherited;
  FAutoCommit := False;
end;

procedure TVendaItemController.SetModel;
begin
  inherited;
  FModel := TVendaItemModel.Create;
end;

end.
