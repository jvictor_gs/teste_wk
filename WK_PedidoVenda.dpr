program WK_PedidoVenda;

uses
  Vcl.Forms,
  WK.Lib.Strings in 'Bibliotecas\WK.Lib.Strings.pas',
  untConnection in 'Objetos\untConnection.pas',
  untDataModule.Firedac in 'Objetos\untDataModule.Firedac.pas' {DMFiredac: TDataModule},
  untGridProperties in 'Objetos\untGridProperties.pas',
  untPesquisa in 'Objetos\untPesquisa.pas' {frmPesquisa},
  untModel.Interfaces in 'Interfaces\untModel.Interfaces.pas',
  untController.Interfaces in 'Interfaces\untController.Interfaces.pas',
  untPadrao.Model in 'Models\untPadrao.Model.pas',
  untVenda.Model in 'Models\untVenda.Model.pas',
  untVendaItem.Model in 'Models\untVendaItem.Model.pas',
  untPadrao.Controller in 'Controllers\untPadrao.Controller.pas',
  untVenda.Controller in 'Controllers\untVenda.Controller.pas',
  untVendaItem.Controller in 'Controllers\untVendaItem.Controller.pas',
  untPadrao.View in 'Views\untPadrao.View.pas' {frmCadastroPadrao},
  untPadraoMasterDetail.View in 'Views\untPadraoMasterDetail.View.pas' {frmMasterDetailPadrao},
  untCadVenda.View in 'Views\untCadVenda.View.pas' {frmCadVenda},
  Menu in 'Views\Menu.pas' {frmMenu};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.Run;
end.
