unit untPadraoMasterDetail.View;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untPadrao.View, Data.DB,
  System.ImageList, Vcl.ImgList, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  untController.Interfaces, untGridProperties, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfrmMasterDetailPadrao = class(TfrmCadastroPadrao)
    pnlDetail: TPanel;
    pnlControlesDetail: TPanel;
    fpOrganizadorControlesDetail: TFlowPanel;
    btnNovoDetail: TSpeedButton;
    btnAlterarDetail: TSpeedButton;
    btnExcluirDetail: TSpeedButton;
    acNovoDetail: TAction;
    acAlterarDetail: TAction;
    acExcluirDetail: TAction;
    acSalvarDetail: TAction;
    acCancelarDetail: TAction;
    pnlDadosDetail: TPanel;
    dsDetail: TDataSource;
    pnlMaster: TPanel;
    grdDetail: TDBGrid;
    procedure acAlterarExecute(Sender: TObject);
    procedure acAlterarDetailExecute(Sender: TObject);
    procedure acNovoDetailExecute(Sender: TObject);
    procedure acExcluirDetailExecute(Sender: TObject);
    procedure acSalvarDetailExecute(Sender: TObject);
    procedure acCancelarDetailExecute(Sender: TObject);
    procedure dsCadastroStateChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure grdDetailKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure AtualizarControlesDetail(State: TDataSetState);
  public
    { Public declarations }
    FControllerDetail: IController;
    FIDDetail: string;
    FChildTable: string;
    FGridDetailProperties: TGridProperties;
    procedure BeforePost; override;
    procedure AfterPost; override;

    procedure SetControllerDetail; virtual; abstract;
    procedure SetIDDetail; virtual;
    procedure SetChildTable; virtual; abstract;
    procedure SetQueryDetailProperties; virtual; abstract;
    procedure AbrirDataSetDetail; virtual;

    procedure BeforePostDetail; virtual; abstract;
    procedure AfterPostDetail; virtual; abstract;
    procedure BeforeDeleteDetail; virtual; abstract;
    procedure AfterDeleteDetail; virtual; abstract;
    procedure NewRecordDetail; virtual;
  end;

var
  frmMasterDetailPadrao: TfrmMasterDetailPadrao;

implementation

uses WK.Lib.Strings, System.Math;

{$R *.dfm}

{ TfrmMasterDetailPadrao }

procedure TfrmMasterDetailPadrao.AbrirDataSetDetail;
var
  vPrimaryKeyMaster, vPrimaryKeyDetail, vCondicaoPesquisa: string;
begin
  vPrimaryKeyMaster := FController.GetPrimaryKey(FTable);
  vPrimaryKeyDetail := FControllerDetail.GetPrimaryKey(FChildTable);
  if dsCadastro.State = dsInsert then
    vCondicaoPesquisa := vPrimaryKeyDetail + ' <> ' + vPrimaryKeyDetail
  else
    vCondicaoPesquisa := vPrimaryKeyMaster + ' = ' +
                          dsCadastro.DataSet.FieldByName(vPrimaryKeyMaster).AsString;

  dsDetail.DataSet := nil;
  dsDetail.DataSet := FControllerDetail.CreateDataSet(
                   FControllerDetail.GetSelectInstruction(vCondicaoPesquisa));
  dsDetail.DataSet.Open;
end;

procedure TfrmMasterDetailPadrao.acAlterarDetailExecute(Sender: TObject);
const
  cMessage = 'Nenhum registro foi selecionado!';
begin
  Self.SetIDDetail;
  if not IsEmptyStr(FIDDetail) then
  begin
    dsDetail.DataSet.Edit;
    Self.AtualizarControlesDetail(dsDetail.State);
  end
  else
    Application.MessageBox(cMessage, 'Aten��o', MB_IconWarning);
end;

procedure TfrmMasterDetailPadrao.acAlterarExecute(Sender: TObject);
begin
  if dsDetail.State in [dsInsert, dsEdit] then
    dsDetail.DataSet.Cancel;
  inherited;
end;

procedure TfrmMasterDetailPadrao.acCancelarDetailExecute(Sender: TObject);
begin
  inherited;
  dsDetail.DataSet.Cancel;
  Self.AtualizarControlesDetail(dsDetail.State);
end;

procedure TfrmMasterDetailPadrao.acExcluirDetailExecute(Sender: TObject);
const
  cMessage = 'Nenhum registro foi selecionado!';
  cConfirmation = 'Deseja realmente excluir?';
begin
  Self.SetIDDetail;
  if not IsEmptyStr(FIDDetail) then
  begin
    if Application.MessageBox(
      cConfirmation, 'Confirma��o', MB_IConQuestion + MB_YesNo) = IdYes then
    begin
      Self.BeforeDeleteDetail;
      try
        FControllerDetail.Delete(dsDetail.DataSet, FChildTable);
        dsDetail.DataSet.Delete;
        Self.AfterPostDetail;
        Self.AtualizarControlesDetail(dsDetail.State);
      except on E: Exception do
        Application.MessageBox(PWideChar(E.Message), 'Erro', MB_IconError);
      end;
    end;
  end
  else
    Application.MessageBox(cMessage, 'Aten��o', MB_IconWarning);
end;

procedure TfrmMasterDetailPadrao.acNovoDetailExecute(Sender: TObject);
begin
  Self.NewRecordDetail;
  Self.AtualizarControlesDetail(dsDetail.State);
end;

procedure TfrmMasterDetailPadrao.acSalvarDetailExecute(Sender: TObject);
begin
  Self.NextFocus;
  Self.BeforePostDetail;
  try
    if dsDetail.State = dsInsert then
      FControllerDetail.Insert(dsDetail.DataSet, FChildTable)
    else if dsDetail.State = dsEdit then
      FControllerDetail.Update(dsDetail.DataSet, FChildTable);

    dsDetail.DataSet.Post;
    Self.AtualizarControlesDetail(dsDetail.State);
    Self.AfterPostDetail;
  except on E: Exception do
    Application.MessageBox(PWideChar(E.Message), 'Erro', MB_IconError);
  end;
end;

procedure TfrmMasterDetailPadrao.AfterPost;
begin
  inherited;
  FControllerDetail.ApplyUpdates;
end;

procedure TfrmMasterDetailPadrao.AtualizarControlesDetail(State: TDataSetState);
var
  vHabilitar: Boolean;
begin
  case State of
    dsInactive, dsBrowse:
      begin
        btnNovoDetail.Action := acNovoDetail;
        btnNovoDetail.Caption := 'N&ovo';
        btnAlterarDetail.Action := acAlterarDetail;
        btnAlterarDetail.Caption := 'A&lterar';
      end;
    dsInsert, dsEdit:
      begin
        btnNovoDetail.Action := acSalvarDetail;
        btnNovoDetail.Caption := 'Sal&var';
        btnAlterarDetail.Action := acCancelarDetail;
        btnAlterarDetail.Caption := 'Ca&ncelar';
      end;
  end;
  vHabilitar := not (State in [dsInsert, dsEdit]);
  pnlDadosDetail.Enabled := not vHabilitar;
  acExcluirDetail.Enabled := vHabilitar;
end;

procedure TfrmMasterDetailPadrao.BeforePost;
const
  cMensagem = 'Conclua a altera��o do item antes de salvar o cadastro!';
begin
  if dsDetail.State in [dsInsert, dsEdit] then
  begin
    Application.MessageBox(cMensagem, 'Aten��o', MB_IconWarning);
    Abort;
  end;
  inherited;
end;

procedure TfrmMasterDetailPadrao.dsCadastroStateChange(Sender: TObject);
begin
  inherited;
  if dsCadastro.State in [dsInsert, dsEdit] then
  begin
    Self.AbrirDataSetDetail;
    Self.ConfigurarGrid(grdDetail, FGridDetailProperties);
  end;
end;

procedure TfrmMasterDetailPadrao.FormCreate(Sender: TObject);
begin
  inherited;
  Self.SetControllerDetail;
  Self.SetChildTable;
  Self.SetQueryDetailProperties;
end;

procedure TfrmMasterDetailPadrao.grdDetailKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if key = VK_RETURN then
    acAlterarDetailExecute(Self);
  if key = VK_DELETE then
    acExcluirDetailExecute(Self);
end;

procedure TfrmMasterDetailPadrao.NewRecordDetail;
begin
  dsDetail.DataSet.Append;
  dsDetail.DataSet.FieldByName(FController.GetPrimaryKey(FTable)).AsString :=
    dsCadastro.DataSet.FieldByName(FController.GetPrimaryKey(FTable)).AsString;
end;

procedure TfrmMasterDetailPadrao.SetIDDetail;
var
  vPrimaryKey: string;
begin
  vPrimaryKey := FControllerDetail.GetPrimaryKey(FChildTable);
  FIDDetail := dsDetail.DataSet.FieldByName(vPrimaryKey).AsString;
end;

end.
