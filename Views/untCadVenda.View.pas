unit untCadVenda.View;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untPadraoMasterDetail.View, Data.DB,
  System.ImageList, Vcl.ImgList, System.Actions, Vcl.ActnList, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask,
  Vcl.DBCtrls, untVenda.Controller, untVendaItem.Controller, System.DateUtils,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfrmCadVenda = class(TfrmMasterDetailPadrao)
    edtTotalVenda: TDBEdit;
    lblTotalVenda: TLabel;
    lblDataVenda: TLabel;
    edtDataVenda: TDBEdit;
    edtDescricao: TDBEdit;
    lblDescricao: TLabel;
    lblCodCompra: TLabel;
    edtIdVenda: TDBEdit;
    lblProduto: TLabel;
    edtIdProduto: TDBEdit;
    edtProduto: TDBEdit;
    lblQuantidade: TLabel;
    edtQuantidade: TDBEdit;
    lblValorUnitario: TLabel;
    edtValorUnitario: TDBEdit;
    lblValorTotal: TLabel;
    edtValorTotal: TDBEdit;
    btnPesquisaProduto: TButton;
    edtNumeroPedido: TDBEdit;
    lblNumeroPedido: TLabel;
    EdtIdCliente: TDBEdit;
    btnPesquisaCliente: TButton;
    EdtNome: TDBEdit;
    lblCliente: TLabel;
    procedure edtQuantidadeExit(Sender: TObject);
    procedure edtValorUnitarioExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnPesquisaProdutoClick(Sender: TObject);
    procedure btnPesquisaClienteClick(Sender: TObject);
    procedure acNovoDetailExecute(Sender: TObject);
    procedure acNovoExecute(Sender: TObject);
    procedure acAlterarDetailExecute(Sender: TObject);
  private
    { Private declarations }
    oVenda: TVendaController;
    oVendaItem: TVendaItemController;

    procedure CalcularTotalItem;
    procedure AtualizarTotalVenda;
    function DescricaoPadrao(pNumeroPedido: Integer): string;
  public
    { Public declarations }
    procedure SetController; override;
    procedure SetFieldSearch; override;
    procedure SetTable; override;
    procedure SetQueryProperties; override;
    procedure BeforePost; override;
    procedure AfterPost; override;
    procedure BeforeDelete; override;
    procedure AfterDelete; override;
    procedure BeforePostDetail; override;
    procedure AfterPostDetail; override;
    procedure BeforeDeleteDetail; override;
    procedure AfterDeleteDetail; override;
    procedure NewRecord; override;

    procedure SetControllerDetail; override;
    procedure SetChildTable; override;
    procedure SetQueryDetailProperties; override;
  end;

var
  frmCadVenda: TfrmCadVenda;

implementation

uses
  untGridProperties, untPesquisa;

{$R *.dfm}

{ TfrmCadVenda }

procedure TfrmCadVenda.acAlterarDetailExecute(Sender: TObject);
begin
  inherited;
  edtIdProduto.SetFocus;
end;

procedure TfrmCadVenda.acNovoDetailExecute(Sender: TObject);
begin
  inherited;
  if dsDetail.DataSet.State in [dsInsert, dsEdit] then
    edtIdProduto.SetFocus;
end;

procedure TfrmCadVenda.acNovoExecute(Sender: TObject);
begin
  inherited;
  if dsCadastro.DataSet.State in [dsInsert, dsEdit] then
    EdtIdCliente.SetFocus;
end;

procedure TfrmCadVenda.AfterDelete;
begin
  inherited;

end;

procedure TfrmCadVenda.AfterDeleteDetail;
begin
  inherited;
  Self.AtualizarTotalVenda;
end;

procedure TfrmCadVenda.AfterPost;
begin
  inherited;

end;

procedure TfrmCadVenda.AfterPostDetail;
begin
  inherited;
  Self.AtualizarTotalVenda;
end;

procedure TfrmCadVenda.AtualizarTotalVenda;
begin
  dsCadastro.DataSet.FieldByName('TOTALVENDA').Value :=
    oVendaItem.CalcularTotalVenda(dsDetail.DataSet);
end;

procedure TfrmCadVenda.BeforeDelete;
begin
  inherited;

end;

procedure TfrmCadVenda.BeforeDeleteDetail;
begin
  inherited;

end;

procedure TfrmCadVenda.BeforePost;
begin
  inherited;

end;

procedure TfrmCadVenda.BeforePostDetail;
begin
  inherited;

end;

procedure TfrmCadVenda.btnPesquisaClienteClick(Sender: TObject);
var
  frmPesquisa: TfrmPesquisa;
begin
  inherited;
  try
     frmPesquisa := TfrmPesquisa.Create(Self, ['IDCLIENTE', 'NOME', 'CIDADE', 'UF'], 'CLIENTE');
     frmPesquisa.SetFieldSearch('NOME', 'Nome');
     frmPesquisa.ShowModal;
     if frmPesquisa.ModalResult = mrYes then
     begin
        dsCadastro.DataSet.FieldByName('IDCLIENTE').AsString := frmPesquisa.FSearchResult[0];
        dsCadastro.DataSet.FieldByName('NOME').AsString := frmPesquisa.FSearchResult[1];
     end;
  finally
    FreeAndNil(frmPesquisa);
    Self.NextFocus;
  end;
end;

procedure TfrmCadVenda.btnPesquisaProdutoClick(Sender: TObject);
var
  frmPesquisa: TfrmPesquisa;
begin
  inherited;
  try
    frmPesquisa := TfrmPesquisa.Create(Self, ['IDPRODUTO', 'DESCRICAO', 'PRECOVENDA'], 'PRODUTO');
    frmPesquisa.SetFieldSearch('DESCRICAO', 'Descri��o');
    frmPesquisa.ShowModal;
    if frmPesquisa.ModalResult = mrYes then
    begin
      dsDetail.DataSet.FieldByName('IDPRODUTO').AsString := frmPesquisa.FSearchResult[0];
      dsDetail.DataSet.FieldByName('PRODUTO').AsString := frmPesquisa.FSearchResult[1];
      dsDetail.DataSet.FieldByName('VALORUNITARIO').AsString := frmPesquisa.FSearchResult[2];
    end;
  finally
    FreeAndNil(frmPesquisa);
    Self.NextFocus;
  end;
end;

procedure TfrmCadVenda.CalcularTotalItem;
begin
  dsDetail.DataSet.FieldByName('VALORTOTAL').Value :=
    oVendaItem.CalcularTotalItem(dsDetail.DataSet);
end;

function TfrmCadVenda.DescricaoPadrao(pNumeroPedido: Integer): string;
const
  cDescricao = 'Pedido de Venda N� %s.';
begin
  Result := Format(cDescricao, [IntToStr(pNumeroPedido)]);
end;

procedure TfrmCadVenda.edtQuantidadeExit(Sender: TObject);
begin
  inherited;
  Self.CalcularTotalItem;
end;

procedure TfrmCadVenda.edtValorUnitarioExit(Sender: TObject);
begin
  inherited;
  Self.CalcularTotalItem;
end;

procedure TfrmCadVenda.FormCreate(Sender: TObject);
begin
  inherited;
  oVenda := TVendaController.Create;
  oVendaItem := TVendaItemController.Create;
end;

procedure TfrmCadVenda.NewRecord;
const
  cBaseNumPedido = 10000;
begin
  inherited;
  dsCadastro.DataSet.FieldByName('NUMEROPEDIDO').AsInteger := cBaseNumPedido +
    dsCadastro.DataSet.FieldByName('IDVENDA').AsInteger;
  dsCadastro.DataSet.FieldByName('DATAVENDA').AsDateTime := DateOf(Now);
  dsCadastro.DataSet.FieldByName('DESCRICAO').AsString :=
    Self.DescricaoPadrao(dsCadastro.DataSet.FieldByName('NUMEROPEDIDO').AsInteger);
  dsCadastro.DataSet.FieldByName('IDCLIENTE').AsVariant := null;
end;

procedure TfrmCadVenda.SetChildTable;
begin
  inherited;
  FChildTable := 'VENDAITEM';
end;

procedure TfrmCadVenda.SetController;
begin
  inherited;
  FController := TVendaController.Create;
end;

procedure TfrmCadVenda.SetControllerDetail;
begin
  inherited;
  FControllerDetail := TVendaItemController.Create;
end;

procedure TfrmCadVenda.SetFieldSearch;
begin
  FFieldSearch := 'DESCRICAO';
  inherited;
end;

procedure TfrmCadVenda.SetQueryDetailProperties;
begin
  inherited;
  FGridDetailProperties := TGridProperties.Create;
  FGridDetailProperties.Add('IDPRODUTO', 'C�d. Produto', 100);
  FGridDetailProperties.Add('PRODUTO', 'Produto', 150);
  FGridDetailProperties.Add('QUANTIDADE', 'Quantidade', 100);
  FGridDetailProperties.Add('VALORUNITARIO', 'Valor Unit�rio', 100);
  FGridDetailProperties.Add('VALORTOTAL', 'Valor Total', 100);
end;

procedure TfrmCadVenda.SetQueryProperties;
begin
  inherited;
  FGridProperties := TGridProperties.Create;
  FGridProperties.Add('IDVENDA', 'C�d.', 50);
  FGridProperties.Add('DESCRICAO', 'Descri��o', 200);
  FGridProperties.Add('IDCLIENTE', 'C�d. Cliente', 100);
  FGridProperties.Add('NOME', 'Nome Cliente', 200);
  FGridProperties.Add('DATAVENDA', 'Data Venda', 100);
  FGridProperties.Add('TOTALVENDA', 'Total Venda', 100);
end;

procedure TfrmCadVenda.SetTable;
begin
  inherited;
  FTable := 'VENDA';
end;

end.
