unit Menu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.ImageList, Vcl.ImgList,
  System.Actions, Vcl.ActnList, Vcl.Buttons, Vcl.WinXCtrls, Vcl.ExtCtrls,
  Vcl.CategoryButtons, Vcl.Imaging.jpeg, FireDAC.Phys.MySQLDef, FireDAC.UI.Intf,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Stan.Intf, FireDAC.Phys,
  FireDAC.Phys.MySQL;

type
  TfrmMenu = class(TForm)
    MenuActions: TActionList;
    MenuIcons: TImageList;
    acMenu: TAction;
    acProcessos: TAction;
    pnlBackGrounf: TPanel;
    Wallpaper: TImage;
    svMenu: TSplitView;
    pnlCabecalho: TPanel;
    btnHome: TSpeedButton;
    svSubMenuSistema: TSplitView;
    fpSubMenuSistema: TFlowPanel;
    btnProcessos: TSpeedButton;
    SpeedButton3: TSpeedButton;
    acVenda: TAction;
    procedure acMenuExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acProcessosExecute(Sender: TObject);
    procedure acVendaExecute(Sender: TObject);
  private
    procedure FecharTodosSplitViews;
    procedure AtivarDesativarSplitView(Sender: TSplitView);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenu: TfrmMenu;

implementation

uses
  untCadVenda.View;

{$R *.dfm}


procedure TfrmMenu.acMenuExecute(Sender: TObject);
begin
  if svMenu.Opened then
    Self.FecharTodosSplitViews
  else
    Self.AtivarDesativarSplitView(svMenu);
end;

procedure TfrmMenu.acProcessosExecute(Sender: TObject);
begin
  Self.AtivarDesativarSplitView(svSubMenuSistema);
end;

procedure TfrmMenu.acVendaExecute(Sender: TObject);
var
  vFrm: TfrmCadVenda;
begin
  try
    vFrm := TfrmCadVenda.Create(Application);
    vFrm.ShowModal;
  finally
    FreeAndNil(vFrm);
  end;
end;

procedure TfrmMenu.FormCreate(Sender: TObject);
begin
  Self.FecharTodosSplitViews;
end;

procedure TfrmMenu.FecharTodosSplitViews;
begin
  svMenu.Close;
  svSubMenuSistema.Close;
end;

procedure TfrmMenu.AtivarDesativarSplitView(Sender: TSplitView);
begin
  if Sender.Opened then
    Sender.Close
  else
    Sender.Open;
end;

end.
