unit untPadrao.View;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls, Vcl.Buttons, System.Actions, Vcl.ActnList,
  System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls,
  untPadrao.Controller, untController.Interfaces, untGridProperties,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type

  TfrmCadastroPadrao = class(TForm)
    pnlBackGround: TPanel;
    pnlControles: TPanel;
    pgDados: TPageControl;
    tsPesquisa: TTabSheet;
    tsDetalhes: TTabSheet;
    grdPesquisa: TDBGrid;
    dsCadastro: TDataSource;
    fpOrganizadorControles: TFlowPanel;
    btnNovo: TSpeedButton;
    btnAlterar: TSpeedButton;
    btnExcluir: TSpeedButton;
    btnFechar: TSpeedButton;
    acControles: TActionList;
    imgControlesIcons: TImageList;
    acNovo: TAction;
    acAlterar: TAction;
    acExcluir: TAction;
    acFechar: TAction;
    acSalvar: TAction;
    acCancelar: TAction;
    pnlPesquisa: TPanel;
    gbxPesquisa: TGroupBox;
    Nome: TLabel;
    rbtnIniciando: TRadioButton;
    rbtnContendo: TRadioButton;
    btnTodos: TButton;
    btnPesquisar: TButton;
    edtPesquisa: TEdit;
    procedure acNovoExecute(Sender: TObject);
    procedure pgDadosChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnTodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acSalvarExecute(Sender: TObject);
    procedure acFecharExecute(Sender: TObject);
    procedure acExcluirExecute(Sender: TObject);
    procedure acAlterarExecute(Sender: TObject);
    procedure acCancelarExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure grdPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }

    type TTipoPesquisa = (tpVazia, tpEspecifica, tpTodos);

    function GetCondicao(Opcao: TTipoPesquisa): string; virtual;
    procedure Pesquisar(Opcao: TTipoPesquisa); overload;
    procedure Pesquisar(Condicao: string); overload;
    procedure AtualizarControles(State: TDataSetState);
    procedure HabilitarComponentes;
    procedure DesabilitarComponentes;

  public
    { Public declarations }

    FController: IController;
    FID: string;
    FTable: string;
    FFieldSearch: string;
    FGridProperties: TGridProperties;

    procedure NextFocus;
    procedure PreviousFocus;

    procedure SetController; virtual; abstract;
    procedure SetFieldSearch; virtual;
    procedure SetID; virtual;
    procedure SetTable; virtual; abstract;
    procedure SetQueryProperties; virtual; abstract;
    procedure AbrirDataSet; virtual;
    procedure ConfigurarGrid(Sender: TDBGrid; Properties: TGridProperties);

    procedure BeforePost; virtual; abstract;
    procedure AfterPost; virtual; abstract;
    procedure BeforeDelete; virtual; abstract;
    procedure AfterDelete; virtual; abstract;
    procedure NewRecord; virtual;
  end;

var
  frmCadastroPadrao: TfrmCadastroPadrao;

implementation

uses WK.Lib.Strings;

{$R *.dfm}

procedure TfrmCadastroPadrao.AbrirDataSet;
begin
  dsCadastro.DataSet := nil;
  dsCadastro.DataSet := FController.CreateDataSet(FController.GetSelectInstruction);
  dsCadastro.DataSet.Open;
end;

procedure TfrmCadastroPadrao.acNovoExecute(Sender: TObject);
begin
  Self.NewRecord;
  Self.AtualizarControles(dsCadastro.State);
end;

procedure TfrmCadastroPadrao.acAlterarExecute(Sender: TObject);
const
  Mensagem = 'Nenhum registro foi selecionado!';
begin
  Self.SetID;
  if not IsEmptyStr(FID) then
  begin
    dsCadastro.DataSet.Edit;
    Self.AtualizarControles(dsCadastro.State);
  end
  else
    Application.MessageBox(Mensagem, 'Aten��o', MB_IconWarning);
end;

procedure TfrmCadastroPadrao.acSalvarExecute(Sender: TObject);
begin
  Self.NextFocus;
  Self.BeforePost;
  try
    if dsCadastro.State = dsInsert then
      FController.Insert(dsCadastro.DataSet, FTable)
    else if dsCadastro.State = dsEdit then
      FController.Update(dsCadastro.DataSet, FTable);

    dsCadastro.DataSet.Post;
    Self.AtualizarControles(dsCadastro.State);
  except on E: Exception do
    Application.MessageBox(PWideChar(E.Message), 'Erro', MB_IconError);
  end;
  Self.AfterPost;
end;

procedure TfrmCadastroPadrao.acCancelarExecute(Sender: TObject);
begin
  dsCadastro.DataSet.Cancel;
  Self.AtualizarControles(dsCadastro.State);
end;

procedure TfrmCadastroPadrao.acExcluirExecute(Sender: TObject);
const
  cMessage = 'Nenhum registro foi selecionado!';
  cConfirmation = 'Deseja realmente excluir?';
begin
  Self.SetID;
  if not IsEmptyStr(FID) then
  begin
    if Application.MessageBox(
      cConfirmation, 'Confirma��o', MB_IConQuestion + MB_YesNo) = IdYes then
    begin
      Self.BeforeDelete;
      try
        FController.Delete(dsCadastro.DataSet, FTable);
      except on E: Exception do
        Application.MessageBox(PWideChar(E.Message), 'Erro', MB_IconError);
      end;
      dsCadastro.DataSet.Delete;
      Self.AfterDelete;
      Self.AtualizarControles(dsCadastro.State);
    end;
  end
  else
    Application.MessageBox(cMessage, 'Aten��o', MB_IconWarning);
end;

procedure TfrmCadastroPadrao.acFecharExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmCadastroPadrao.AtualizarControles(State: TDataSetState);
var
  Habilitar: Boolean;
begin
  case State of
    dsInactive, dsBrowse:
      begin
        pgDados.ActivePage := tsPesquisa;
        btnNovo.Action := acNovo;
        btnNovo.Caption := '&Novo';
        btnAlterar.Action := acAlterar;
        btnAlterar.Caption := '&Alterar';
      end;
    dsInsert, dsEdit:
      begin
        pgDados.ActivePage := tsDetalhes;
        btnNovo.Action := acSalvar;
        btnNovo.Caption := '&Salvar';
        btnAlterar.Action := acCancelar;
        btnAlterar.Caption := '&Cancelar';
      end;
  end;
  Habilitar := not (State in [dsInsert, dsEdit]);
  acExcluir.Enabled := Habilitar;
  acFechar.Enabled := Habilitar;
end;

procedure TfrmCadastroPadrao.NextFocus;
begin
  Perform(Wm_NextDlgCtl, 0, 0);
end;

procedure TfrmCadastroPadrao.PreviousFocus;
begin
  Perform(Wm_NextDlgCtl, 1, 0);
end;

procedure TfrmCadastroPadrao.btnPesquisarClick(Sender: TObject);
const
  Mensagem = 'Deve ser informado um valor para realizar a pesquisa!';
begin
  if edtPesquisa.Text = EmptyStr then
  begin
    Application.MessageBox(Mensagem, 'Aten��o', MB_IConWarning);
    Exit;
  end;
  Self.Pesquisar(tpEspecifica);
end;

procedure TfrmCadastroPadrao.btnTodosClick(Sender: TObject);
begin
  Self.Pesquisar(tpTodos);
end;

procedure TfrmCadastroPadrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dsCadastro.DataSet.Close;
  FreeAndNil(FGridProperties);
end;

procedure TfrmCadastroPadrao.FormCreate(Sender: TObject);
begin
  Self.SetController;
  Self.SetFieldSearch;
  Self.SetTable;
  Self.SetQueryProperties;
end;

procedure TfrmCadastroPadrao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    Self.NextFocus;
    key := #0;
  end
  else if key = #27 then
  begin
    Self.PreviousFocus;
    key := #0;
  end;
end;

procedure TfrmCadastroPadrao.FormShow(Sender: TObject);
begin
  Self.DesabilitarComponentes;
  try
    pgDados.ActivePage := tsPesquisa;
    Self.AbrirDataSet;
    Self.ConfigurarGrid(grdPesquisa, FGridProperties);
  finally
    Self.HabilitarComponentes;
  end;
  edtPesquisa.SetFocus;
end;

procedure TfrmCadastroPadrao.pgDadosChange(Sender: TObject);
begin
  tsPesquisa.Enabled := not (dsCadastro.State in [dsInsert, dsEdit]);
  tsDetalhes.Enabled := (dsCadastro.State in [dsInsert, dsEdit]);
end;

procedure TfrmCadastroPadrao.SetFieldSearch;
const
  cError = 'O Field para pesquisa deve ser configurado.';
begin
  if IsEmptyStr(FFieldSearch) then
    raise Exception.Create(cError);
end;

procedure TfrmCadastroPadrao.SetID;
var
  PrimaryKey: string;
begin
  inherited;
  PrimaryKey := FController.GetPrimaryKey(FTable);
  FID := dsCadastro.DataSet.FieldByName(PrimaryKey).AsString;
end;

function TfrmCadastroPadrao.GetCondicao(Opcao: TTipoPesquisa): string;
begin
  case opcao of
    tpVazia:
      begin
        Result := '1<>1';
      end;
    tpEspecifica:
      begin
        Result := FFieldSearch + ' like ';
        if rbtnIniciando.Checked then
          Result := Result + QuotedStr(edtPesquisa.Text + '%')
        else
          Result := Result + QuotedStr('%' + edtPesquisa.Text + '%');
      end;
    tpTodos:
      begin
        Result := EmptyStr;
      end;
  end;
end;

procedure TfrmCadastroPadrao.grdPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    acAlterarExecute(Self);
  if key = VK_DELETE then
    acExcluirExecute(Self);
end;

procedure TfrmCadastroPadrao.HabilitarComponentes;
begin
  pgDados.Enabled := True;
  pnlControles.Enabled := True;
end;

procedure TfrmCadastroPadrao.NewRecord;
var
  PrimaryKey: string;
begin
  dsCadastro.DataSet.Append;
  PrimaryKey := FController.GetPrimaryKey(FTable);
  dsCadastro.DataSet.FieldByName(PrimaryKey).Value := FController.GetSeq(FTable);
end;

procedure TfrmCadastroPadrao.DesabilitarComponentes;
begin
  pgDados.Enabled := False;
  pnlControles.Enabled := False;
end;

procedure TfrmCadastroPadrao.Pesquisar(Opcao: TTipoPesquisa);
var
  Condicao: string;
begin
  Condicao := Self.GetCondicao(Opcao);
  Self.Pesquisar(Condicao);
end;

procedure TfrmCadastroPadrao.Pesquisar(Condicao: string);
begin
  Self.DesabilitarComponentes;
  try
    if dsCadastro.DataSet.Active then
      dsCadastro.DataSet.Close;

    dsCadastro.DataSet := FController.CreateDataSet(
               FController.GetSelectInstruction(Condicao));
    dsCadastro.DataSet.Open;

    Self.ConfigurarGrid(grdPesquisa, FGridProperties);
  finally
    Self.HabilitarComponentes;
  end;
end;

procedure TfrmCadastroPadrao.ConfigurarGrid(Sender: TDBGrid; Properties: TGridProperties);
var
  I: Byte;
  Index: Integer;
  VisibleFields: TStringList;
begin
  VisibleFields := Properties.GetFields;
  for I := 0 to Pred(Sender.Columns.Count) do
  begin
    Index := VisibleFields.IndexOf(Sender.Columns.Items[I].FieldName);
    if Index <> -1 then
    begin
      Sender.Columns.Items[I].Visible := True;
      Sender.Columns.Items[I].Width := StrToInt(Properties.ExtractDisplayLength(Index));
      Sender.Columns.Items[I].Title.Caption := Properties.ExtractDisplayLabel(Index);
    end
    else
      Sender.Columns.Items[I].Visible := False;
  end;
end;

end.
