inherited frmCadVenda: TfrmCadVenda
  Caption = 'Pedido de Venda'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBackGround: TPanel
    inherited pgDados: TPageControl
      inherited tsPesquisa: TTabSheet
        inherited pnlPesquisa: TPanel
          inherited gbxPesquisa: TGroupBox
            inherited Nome: TLabel
              Width = 46
              Caption = 'Descricao'
              ExplicitWidth = 46
            end
          end
        end
      end
      inherited tsDetalhes: TTabSheet
        inherited pnlDetail: TPanel
          inherited pnlDadosDetail: TPanel
            object lblProduto: TLabel
              Left = 38
              Top = 11
              Width = 38
              Height = 13
              Caption = 'Produto'
            end
            object lblQuantidade: TLabel
              Left = 20
              Top = 38
              Width = 56
              Height = 13
              Caption = 'Quantidade'
            end
            object lblValorUnitario: TLabel
              Left = 209
              Top = 38
              Width = 64
              Height = 13
              Caption = 'Valor Unit'#225'rio'
            end
            object lblValorTotal: TLabel
              Left = 406
              Top = 38
              Width = 51
              Height = 13
              Caption = 'Valor Total'
            end
            object edtIdProduto: TDBEdit
              Left = 82
              Top = 8
              Width = 100
              Height = 21
              DataField = 'IDPRODUTO'
              DataSource = dsDetail
              TabOrder = 0
            end
            object edtProduto: TDBEdit
              Left = 203
              Top = 8
              Width = 381
              Height = 21
              DataField = 'PRODUTO'
              DataSource = dsDetail
              Enabled = False
              TabOrder = 2
            end
            object edtQuantidade: TDBEdit
              Left = 82
              Top = 35
              Width = 121
              Height = 21
              DataField = 'QUANTIDADE'
              DataSource = dsDetail
              TabOrder = 3
              OnExit = edtQuantidadeExit
            end
            object edtValorUnitario: TDBEdit
              Left = 279
              Top = 35
              Width = 121
              Height = 21
              DataField = 'VALORUNITARIO'
              DataSource = dsDetail
              TabOrder = 4
              OnExit = edtValorUnitarioExit
            end
            object edtValorTotal: TDBEdit
              Left = 463
              Top = 35
              Width = 121
              Height = 21
              DataField = 'VALORTOTAL'
              DataSource = dsDetail
              Enabled = False
              TabOrder = 5
            end
            object btnPesquisaProduto: TButton
              Left = 182
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              TabOrder = 1
              OnClick = btnPesquisaProdutoClick
            end
          end
          inherited grdDetail: TDBGrid
            TabStop = True
          end
        end
        inherited pnlMaster: TPanel
          object lblTotalVenda: TLabel
            Left = 22
            Top = 123
            Width = 57
            Height = 13
            Caption = 'Total Venda'
          end
          object lblDataVenda: TLabel
            Left = 8
            Top = 96
            Width = 71
            Height = 13
            Caption = 'Data da Venda'
          end
          object lblDescricao: TLabel
            Left = 33
            Top = 69
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object lblCodCompra: TLabel
            Left = 56
            Top = 18
            Width = 23
            Height = 13
            Caption = 'C'#243'd.'
          end
          object lblNumeroPedido: TLabel
            Left = 226
            Top = 18
            Width = 72
            Height = 13
            Caption = 'N'#250'mero Pedido'
          end
          object lblCliente: TLabel
            Left = 46
            Top = 45
            Width = 33
            Height = 13
            Caption = 'Cliente'
          end
          object edtTotalVenda: TDBEdit
            Left = 85
            Top = 120
            Width = 121
            Height = 21
            DataField = 'TOTALVENDA'
            DataSource = dsCadastro
            Enabled = False
            TabOrder = 7
          end
          object edtDataVenda: TDBEdit
            Left = 85
            Top = 93
            Width = 121
            Height = 21
            DataField = 'DATAVENDA'
            DataSource = dsCadastro
            TabOrder = 6
          end
          object edtDescricao: TDBEdit
            Left = 85
            Top = 66
            Width = 340
            Height = 21
            CharCase = ecUpperCase
            DataField = 'DESCRICAO'
            DataSource = dsCadastro
            MaxLength = 30
            TabOrder = 5
          end
          object edtIdVenda: TDBEdit
            Left = 85
            Top = 15
            Width = 121
            Height = 21
            DataField = 'IDVENDA'
            DataSource = dsCadastro
            Enabled = False
            TabOrder = 0
          end
          object edtNumeroPedido: TDBEdit
            Left = 304
            Top = 15
            Width = 121
            Height = 21
            DataField = 'NUMEROPEDIDO'
            DataSource = dsCadastro
            Enabled = False
            TabOrder = 1
          end
          object EdtIdCliente: TDBEdit
            Left = 85
            Top = 42
            Width = 100
            Height = 21
            DataField = 'IDCLIENTE'
            DataSource = dsCadastro
            TabOrder = 2
          end
          object btnPesquisaCliente: TButton
            Left = 185
            Top = 42
            Width = 21
            Height = 21
            Caption = '...'
            TabOrder = 3
            TabStop = False
            OnClick = btnPesquisaClienteClick
          end
          object EdtNome: TDBEdit
            Left = 206
            Top = 42
            Width = 219
            Height = 21
            CharCase = ecUpperCase
            DataField = 'NOME'
            DataSource = dsCadastro
            Enabled = False
            TabOrder = 4
          end
        end
      end
    end
  end
end
