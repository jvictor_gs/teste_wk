unit untPesquisa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, untPadrao.Model;

type
  TfrmPesquisa = class(TForm)
    dsPesquisa: TDataSource;
    pnlBackground: TPanel;
    pnlDadosPesquisa: TPanel;
    DBGrid1: TDBGrid;
    cbxCampos: TComboBox;
    cbxFiltro: TComboBox;
    edtBusca: TEdit;
    btnPesquisar: TButton;
    lblCampo: TLabel;
    lblCondicao: TLabel;
    lblValor: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FModel: TModel;
    FFields, FTable: string;
    FFieldSearch: Array [0..1] of string;

    function GetSearchResult: TStringList;
    function GetCampos(pCampos: array of string): string;

  public
    { Public declarations }
    var
      FSearchResult: TStringList;

    constructor Create(AOwner: TComponent; FieldsList: array of string;
      Table: string); reintroduce;

    procedure SetFieldSearch(Field, Caption: string);

  end;

var
  frmPesquisa: TfrmPesquisa;

implementation

  uses WK.Lib.Strings;

{$R *.dfm}

{ TfrmPesquisa }

procedure TfrmPesquisa.btnPesquisarClick(Sender: TObject);
const
  cSQL = 'SELECT %s FROM %s WHERE %s';
  cMessage = 'Informe um valor para realizar a pesquisa!';
var
  vWhere: string;
begin
  if IsEmptyStr(edtBusca.Text) then
  begin
    Application.MessageBox(cMessage, 'Aten��o', MB_ICONWARNING);
    edtBusca.SetFocus;
    Exit;
  end;

  if cbxFiltro.ItemIndex = 0 then
    vWhere := FFieldSearch[0] + ' like ' + Trim(QuotedStr('%' + edtBusca.Text + '%'))
  else
    vWhere := FFieldSearch[0] + ' like ' + Trim(QuotedStr(edtBusca.Text));

  dsPesquisa.DataSet := FModel.CreateDataSet(Format(cSQL, [FFields, FTable, vWhere]));
end;

constructor TfrmPesquisa.Create(AOwner: TComponent; FieldsList: array of string;
  Table: string);
begin
  inherited Create(AOwner);
  FModel := TModel.Create;
  FTable := Table;
  FFields := GetCampos(FieldsList);
end;

procedure TfrmPesquisa.DBGrid1DblClick(Sender: TObject);
begin
  FSearchResult := Self.GetSearchResult;
  Self.ModalResult := mrYes;
end;

procedure TfrmPesquisa.FormShow(Sender: TObject);
begin
  cbxCampos.Items.Add(FFieldSearch[1]);
  cbxCampos.ItemIndex := 0;
  dsPesquisa.DataSet := FModel.CreateDataSet('SELECT ' + FFields + ' FROM ' + FTable);
end;

function TfrmPesquisa.GetSearchResult: TStringList;
var
  vField: TField;
begin
  Result := TStringList.Create;
  for vField in dsPesquisa.DataSet.Fields do
    Result.Add(dsPesquisa.DataSet.Fields[vField.Index].AsString);
end;

function TfrmPesquisa.GetCampos(pCampos: array of string): string;
var
  I: Byte;
begin
  for I := Low(pCampos) to High(pCampos) do
  begin
    if I = 0 then
      Result := pCampos[I]
    else
      Result := Result + ',' + pCampos[I];
  end;
end;

procedure TfrmPesquisa.SetFieldSearch(Field, Caption: string);
begin
  FFieldSearch[0] := Field;
  FFieldSearch[1] := Caption;
end;

end.
