object frmPesquisa: TfrmPesquisa
  Left = 0
  Top = 0
  Caption = 'Pesquisa'
  ClientHeight = 281
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBackground: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 281
    Align = alClient
    TabOrder = 0
    object pnlDadosPesquisa: TPanel
      Left = 1
      Top = 1
      Width = 632
      Height = 72
      Align = alTop
      Color = 16229644
      ParentBackground = False
      TabOrder = 0
      object lblCampo: TLabel
        Left = 8
        Top = 12
        Width = 33
        Height = 13
        Caption = 'Campo'
      end
      object lblCondicao: TLabel
        Left = 159
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Condi'#231#227'o'
      end
      object lblValor: TLabel
        Left = 310
        Top = 12
        Width = 24
        Height = 13
        Caption = 'Valor'
      end
      object cbxCampos: TComboBox
        Left = 8
        Top = 31
        Width = 145
        Height = 21
        TabOrder = 0
        Text = 'cbxCampos'
      end
      object cbxFiltro: TComboBox
        Left = 159
        Top = 31
        Width = 145
        Height = 21
        ItemIndex = 0
        TabOrder = 1
        Text = 'Contendo'
        Items.Strings = (
          'Contendo'
          'Igual a')
      end
      object edtBusca: TEdit
        Left = 310
        Top = 31
        Width = 227
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
      end
      object btnPesquisar: TButton
        Left = 543
        Top = 29
        Width = 75
        Height = 25
        Caption = 'Pesquisar'
        TabOrder = 3
        OnClick = btnPesquisarClick
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 73
      Width = 632
      Height = 207
      Align = alClient
      DataSource = dsPesquisa
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
    end
  end
  object dsPesquisa: TDataSource
    Left = 576
    Top = 8
  end
end
