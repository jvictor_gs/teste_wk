unit untGridProperties;

interface

uses
  System.Classes, System.SysUtils;

  type IGridProperties = interface
    ['{61536D05-70E0-4E97-AD3D-2FE93EA66A07}']
    function GetFields: TStringList;
    function ExtractFieldName(Value: string): string;
    function ExtractDisplayLabel(Value: string): string;
    function ExtractDisplayLength(Value: string): string;
    procedure Add(CollumnName, DisplayLabel: string; DisplayLength: Integer);
  end;

  type TGridProperties = class(TInterfacedObject, IGridProperties)
    private
      const
        Struct = '%s=%s:%s;';
        DelimiterName = '=';
        DelimiterCaption = ':';
        DelimiterLength = ';';
      var
        FProperties: TStringList;

      function ExtractFieldName(Value: string): string; overload;
      function ExtractDisplayLabel(Value: string): string; overload;
      function ExtractDisplayLength(Value: string): string; overload;

    public
      constructor Create;
      destructor Destroy; override;

      function GetFields: TStringList;
      function ExtractFieldName(Index: Byte): string; overload;
      function ExtractDisplayLabel(Index: Byte): string; overload;
      function ExtractDisplayLength(Index: Byte): string; overload;

      procedure Add(FieldName, DisplayLabel: string; DisplayLength: Integer);
  end;

implementation

{ TQueryProperties }

constructor TGridProperties.Create;
begin
  inherited;
  FProperties := TStringList.Create;
end;

destructor TGridProperties.Destroy;
begin
  FProperties.Free;
  inherited;
end;

procedure TGridProperties.Add(FieldName, DisplayLabel: string;
  DisplayLength: Integer);
begin
  FProperties.Add(Format(Struct, [FieldName, DisplayLabel, IntToStr(DisplayLength)]));
end;

function TGridProperties.ExtractFieldName(Index: Byte): string;
var
  vValue: string;
begin
  vValue := FProperties.Strings[Index];
  Result := ExtractFieldName(vValue);
end;

function TGridProperties.ExtractDisplayLabel(Index: Byte): string;
var
  vValue: string;
begin
  vValue := FProperties.Strings[Index];
  Result := ExtractDisplayLabel(vValue);
end;

function TGridProperties.ExtractDisplayLength(Index: Byte): string;
var
  vValue: string;
begin
  vValue := FProperties.Strings[Index];
  Result := ExtractDisplayLength(vValue);
end;

function TGridProperties.ExtractFieldName(Value: string): string;
var
  vLimit: Byte;
begin
  vLimit := Pos(DelimiterName, Value);
  Result := Copy(Value, 0, Pred(vLimit));
end;

function TGridProperties.ExtractDisplayLabel(Value: string): string;
var
  vBegin, vLimit: Byte;
begin
  vBegin := Pos(DelimiterName, Value);
  vLimit := Pos(DelimiterCaption, Value);
  Result := Copy(Value, Succ(vBegin), Pred(vLimit-vBegin));
end;

function TGridProperties.ExtractDisplayLength(Value: string): string;
var
  vBegin, vLimit: Byte;
begin
  vBegin := Pos(DelimiterCaption, Value);
  vLimit := Pos(DelimiterLength, Value);
  Result := Copy(Value, Succ(vBegin), Pred(vLimit-vBegin));
end;

function TGridProperties.GetFields: TStringList;
var
  I: Byte;
begin
  Result := TStringList.Create;
  for I := 0 to Pred(FProperties.Count) do
    Result.Add(ExtractFieldName(FProperties.Strings[I]));
end;

end.
