unit untConnection;

interface

uses
  FireDAC.Comp.Client, System.Classes;

  type IConexao = interface
    ['{5F0900B6-895E-4D4C-A2B8-543723CEECED}']
    function CreateConnection: TFDConnection;
    function GetConnection: TFDConnection;
    function GetSelectSchemaCollumns(Tabela: string): string;
    function GetPrimaryKey(Tabela: string): string;
    function FieldExists(Tabela: string; FieldName: string): Boolean;
    procedure Execute(Sql: string);
  end;

  type TConexaoMySQL = class(TInterfacedObject, IConexao)
    strict private
      FConnection: TFDConnection;
      function CreateConnection: TFDConnection;
    public
      constructor Create;
      destructor Destroy; override;
      function GetConnection: TFDConnection;
      function GetSelectSchemaCollumns(Tabela: string): string;
      function GetPrimaryKey(Tabela: string): string;
      function FieldExists(Tabela: string; FieldName: string): Boolean;
      procedure Execute(Sql: string);
  end;


implementation

uses
  System.SysUtils;

{ TConexaoMySQL }

constructor TConexaoMySQL.Create;
begin
  FConnection := Self.CreateConnection;
end;

destructor TConexaoMySQL.Destroy;
begin
  FreeAndNil(FConnection);
  inherited;
end;

procedure TConexaoMySQL.Execute(Sql: string);
begin
  FConnection.ExecSQL(Sql);
end;

function TConexaoMySQL.CreateConnection: TFDConnection;
begin
  Result := TFDConnection.Create(nil);
  Result.Params.Clear;
  Result.Params.Add('DriverID=MySQL');
  Result.Params.Add('Server=127.0.0.1');
  Result.Params.Add('Port=3306');
  Result.Params.Add('Database=wk_pedidovenda');
  Result.Params.Add('CharacterSet=utf8');
  Result.Params.Add('User_Name=root');
  Result.Params.Add('Password=');
  Result.Connected := True;
end;

function TConexaoMySQL.GetConnection: TFDConnection;
begin
  Result := FConnection;
end;

function TConexaoMySQL.FieldExists(Tabela, FieldName: string): Boolean;
const
  cInstruction = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS ' +
            'WHERE TABLE_NAME = %s AND COLUMN_NAME = %s;';
var
  vValue: string;
begin
  vValue := FConnection.ExecSQLScalar(Format(cInstruction, [QuotedStr(Tabela), QuotedStr(FieldName)]));
  Result := vValue <> EmptyStr;
end;

function TConexaoMySQL.GetSelectSchemaCollumns(Tabela: string): string;
const
  cInstruction = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS ' +
            'WHERE TABLE_NAME = %s;';
begin
  Result := Format(cInstruction, [QuotedStr(Tabela)]);
end;

function TConexaoMySQL.GetPrimaryKey(Tabela: string): string;
const
  cInstruction = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS ' +
            'WHERE TABLE_NAME = %s AND COLUMN_KEY = ''PRI'';';
var
  vValue: string;
begin
  vValue := FConnection.ExecSQLScalar(Format(cInstruction, [QuotedStr(Tabela)]));
  Result := vValue;
end;

end.
