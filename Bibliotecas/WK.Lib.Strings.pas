unit WK.Lib.Strings;

interface

uses
  System.SysUtils, Data.DB, Winapi.Windows, Vcl.Forms;

  function IsEmptyStr(Value: string): Boolean;
  function ConvertToString(Value: Variant): String; overload;
  function ConvertToString(Field: TField): String; overload;
  function FormatStringToDataBase(Value: string; FieldType: TFieldType): string;
  function IsValidDate(Value: string; ShowMessage: Boolean = False): Boolean;

implementation

function IsEmptyStr(Value: string): Boolean;
begin
  Result := Trim(Value) = '';
end;

function IsValidDate(Value: string; ShowMessage: Boolean = False): Boolean;
const
  cMessage = 'O valor informado n�o � uma data v�lida.' + sLineBreak +
    'Utilize o formato dd/mm/aaaa!';
var
  vDate: TDateTime;
begin
  Result := TryStrToDate(Value, vDate);
  if (not Result) and ShowMessage then
    Application.MessageBox(cMessage, 'Aten��o', MB_ICONWARNING);
end;

function FormatStringToDataBase(Value: string; FieldType: TFieldType): string;
begin
  case FieldType of
    ftDateTime:
      begin
        Result := QuotedStr(FormatDateTime('yyyy-mm-dd hh:mm:ss', StrToDateTime(Value)));
      end;
    ftDate, ftTime, ftString, ftWord, ftWideString, ftLongWord:
      begin
        Result := QuotedStr(Value);
      end;
    ftFloat, ftCurrency, ftBCD:
      begin
        Result := Value.Replace(',', '.');
      end
    else
      Result := Value;
  end;
end;

function ConvertToString(Value: Variant): String;
begin
  Result := '';
  case TVarData(Value).VType of
    varSmallInt, varInteger:
      begin
        Result := IntToStr(Value);
      end;
    varSingle, varDouble, varCurrency:
      begin
        Result := FloatToStr(Value);
      end;
    varDate:
      begin
        Result := FormatDateTime('dd/mm/yyyy', Value);
      end;
    varBoolean:
      begin
        if Value then
          Result := 'True'
        else
          Result := 'False';
      end;
  else
    begin
      if Value = 'Null' then
        Result := ''
      else
        Result := Value;
    end;
  end;
end;

function ConvertToString(Field: TField): String;
begin
  Result := '';
  try
    case Field.DataType of
      ftBoolean:
        begin
          if Field.Value then
            Result := 'True'
          else
            Result := 'False';
        end;
      ftDate, ftTime, ftDateTime:
        begin
          Result := FormatDateTime('dd/mm/yyyy', Field.Value);
        end;
      ftInteger, ftByte, ftSmallInt, ftShortInt, ftLargeInt:
        begin
          Result := IntToStr(Field.Value);
        end;
      ftSingle, ftCurrency, ftExtended:
        begin
          Result := FloatToStr(Field.Value);
        end;
      ftString, ftWord, ftWideString, ftLongWord:
        begin
          Result := Field.AsString;
        end;
    end;
  except
    Result := '';
  end;
end;

end.
