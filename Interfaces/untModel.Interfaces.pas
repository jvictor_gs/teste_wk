unit untModel.Interfaces;

interface

uses
  FireDAC.Comp.Client, Data.DB;

  type IModel = interface
    ['{B0A91512-D28C-44E8-8645-167D9AB26DC8}']
      function GetSeq(Table: string): Integer;
      function GetPrimaryKey(Table: string): string;
      function CreateConnection: TFDConnection;
      function CreateDataSet(Sql: string): TFDQuery;
      function GetScriptInsert(Sender: TDataSet; Table: string): string;
      function GetScriptUpdate(Sender: TDataSet; Table: string): string;
      function GetScriptDelete(Sender: TDataSet; Table: string): string;

      procedure Execute(Sql: string);
      procedure SetTableAutoIncrement;
  end;

implementation

end.
