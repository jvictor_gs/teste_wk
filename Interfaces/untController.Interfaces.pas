unit untController.Interfaces;

interface

uses
  System.Classes, Data.DB;

  type IController = interface
    ['{3DC86C5A-4A7B-4DA0-A154-0CA1555F08C3}']
      function CreateDataSet(Sql: string): TDataSet;
      function GetSeq(Table: string): Integer;
      function GetPrimaryKey(Table: string): string;
      function AllowInsert(Sender: TDataSet): Boolean;
      function AllowDelete(Id: string): Boolean;
      function GetRequiredFields: TStringList;
      function GetSelectInstruction(Where: string = ''): string;
      procedure SetAutoCommit;
      procedure Insert(Sender: TDataSet; Table: string);
      procedure Update(Sender: TDataSet; Table: string);
      procedure Delete(Sender: TDataSet; Table: string);
      procedure ApplyUpdates;
  end;

implementation

end.
